import 'package:flutter/material.dart';
import 'package:path/path.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column();
  }
}

class About extends StatelessWidget {
  const About({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('ABOUT'),
        backgroundColor: Colors.brown,
      ),
      body: Column(
        children: [
          SizedBox(height: 10,),
          CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage("assets/images/2.jpg"),
          ),
          SizedBox(height: 20),
          name(
            text: Text(
              "นาย นราวิชญ์ กิจวงศ์วัฒนา 6350110010",
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold,color: Colors.white),
            ),
          ),
          SizedBox(height: 50),
          CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage("assets/images/1.jpg"),
          ),
          SizedBox(height: 20),
          name(
            text: Text(
              "นางสาว ยลรดี สุขใส  6350110016",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold,color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }
}

class name extends StatelessWidget {
  const name({Key? key, required this.text}) : super(key: key);

  final Text text;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: FlatButton(
        padding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        color: Colors.brown,
        onPressed: () {},
        child: Row(
          children: [
            SizedBox(width: 30),
            Expanded(
              child: text,
            ),
          ],
        ),
      ),
    );
  }
}

class image extends StatelessWidget {
  const image({Key? key, required this.assetImage}) : super(key: key);

  final AssetImage assetImage;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: SizedBox(
        height: 115,
        width: 115,
        child: CircleAvatar(
          backgroundImage: assetImage,
        ),
      ),
    );
  }
}
