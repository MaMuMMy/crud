<?php
header('Content-Type: application/json');

if (isset($_POST['image'])) {
    $image = $_POST['image'];
}else{
    $image = "";
}

$image_name = $_POST['filename'];
$name = $_POST['name'];
$price = $_POST['price'];
$description = $_POST['description'];

$response = array();

$decodedImage = base64_decode($image);

$return = file_put_contents("./upload/".$image_name, $decodedImage);

if($return !== false){
    $host="localhost";      //replace with your hostname 
    $username="root";       //replace with your username 
    $password="";           //replace with your password 
    $db_name="android";     //replace with your database 
    
    //open connection to mysql db
    $connection = new mysqli($host,$username,$password,$db_name);
    
    // mysql inserting a new row
    $sql = "INSERT INTO productpicts(name, price, description, image_name) VALUES('$name', '$price', '$description','$image_name')";
    $result = $connection->query($sql);
    
    //close the db connection
    $connection->close();

    $response['success'] = 1;
    $response['message'] = "Insert data and image file $name has been uploaded";
}else{
    $response['success'] = 0;
    $response['message'] = "There was an error insert and upload the file, please try again!";
}
//set http header with content type as JSON and encoded with utf-8
header("Content-Type: application/json;charset=utf-8");
echo json_encode($response);
?>
