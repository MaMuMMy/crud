<?php
 
/*
 * Following code will delete a product from table
 * A product is identified by product id (pid)
 */
 
// array for JSON response
$response = array();

//set http header with content type as JSON and encoded with utf-8
header("Content-Type: application/json;charset=utf-8");	

// check for required fields
if (!empty($_POST['pid'])) {
    $pid = $_POST['pid'];
 
	$host="localhost";      //replace with your hostname 
	$username="root";       //replace with your username 
	$password="";           //replace with your password 
	$db_name="android";     //replace with your database 

	//open connection to mysql db
	$connection = new mysqli($host,$username,$password,$db_name);
	if($connection->error) {
		$response["success"] = 0;
		$response["message"] = "Error Connection failed " .$connection->error;
 
		// echoing JSON response
		echo json_encode($response);
		//close the db connection
		$connection->close();
		exit();
	}
	if(!$connection->set_charset("utf8")) {  
		$response["success"] = 0;
		$response["message"] = "Error loading character set utf8:". $connection->error;
 
		// echoing JSON response
		echo json_encode($response);
		//close the db connection
		$connection->close();
		exit(); 
	}
 
    // mysql update row with matched pid
	$sql = "DELETE FROM products WHERE pid = $pid";
	$result = $connection->query($sql);
	 
    // check if row deleted or not
    if ($connection->affected_rows > 0) {
        // successfully updated
        $response["success"] = 1;
        $response["message"] = "Product successfully deleted";
 
        // echoing JSON response
        echo json_encode($response);
    } else {
        // no product found
        $response["success"] = 0;
        $response["message"] = "No product found";
 
        // echo no users JSON
        echo json_encode($response);
    }
	//close the db connection
	$connection->close();
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}
?>