<?php
 
/*
 * Following code will list all the products
 */
 
// array for JSON response
$response = array();
 
$host="localhost";      //replace with your hostname 
$username="root";       //replace with your username 
$password="";           //replace with your password 
$db_name="android";     //replace with your database 

//open connection to mysql db
$connection = new mysqli($host,$username,$password,$db_name);
if($connection->error) {
	die("Error Connection failed " .$connection->error);
}
if(!$connection->set_charset("utf8")) {  
	printf("Error loading character set utf8: %s\n", $connection->error);  
	exit();  
}

//set http header with content type as JSON and encoded with utf-8
header("Content-Type: application/json;charset=utf-8");	

// get all products from products table
$sql = "select * from products";
$result = $connection->query($sql); 
 
// check for empty result
if ($result->num_rows > 0) {
    // looping through all results
    // products node
    $response["products"] = array();
 
    while ($row = $result->fetch_assoc()) {
        // temp user array
        $product = array();
        $product["pid"] = $row["pid"];
        $product["name"] = $row["name"];
        $product["price"] = $row["price"];
        $product["description"] = $row["description"];
 
        // push single product into final response array
        array_push($response["products"], $product);
    }
    // success
    $response["success"] = 1;

	// echoing JSON response
    echo json_encode($response);
} else {
    // no products found
    $response["success"] = 0;
    $response["message"] = "No products found";
 
    // echo no users JSON
    echo json_encode($response);
}

//close the db connection
$connection->close();
?>