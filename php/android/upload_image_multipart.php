<?php
header('Content-Type: application/json');
$file_name = basename( $_FILES['uploadedfile']['name']);
$target_path  = "./upload/";//To receive the file directory
$imageFileType = pathinfo($_FILES['uploadedfile']['name'],PATHINFO_EXTENSION);
$image_name = guidv4().".".$imageFileType;

$target_path = $target_path . $image_name;
$name = $_POST['name'];
$price = $_POST['price'];
$description = $_POST['description'];

if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {
    
    $host="localhost";      //replace with your hostname 
    $username="root";       //replace with your username 
    $password="";           //replace with your password 
    $db_name="android";     //replace with your database 
    
    //open connection to mysql db
    $connection = new mysqli($host,$username,$password,$db_name);
    
    // mysql inserting a new row
    $sql = "INSERT INTO productpicts(name, price, description, image_name) VALUES('$name', '$price', '$description','$image_name')";
    $result = $connection->query($sql);
    
    //close the db connection
    $connection->close();

    $arr["StatusID"] = 1;
	$arr["Message"] = "Insert data and image file ".  basename( $_FILES['uploadedfile']['name']). " has been uploaded";
}  else{
	$arr["StatusID"] = 0;
	$arr["Message"] = "There was an error insert and upload the file, please try again!" . $_FILES['uploadedfile']['error'];
}

//set http header with content type as JSON and encoded with utf-8
header("Content-Type: application/json;charset=utf-8");
echo json_encode($arr);

//guidv4 from https://www.uuidgenerator.net/dev-corner/php
function guidv4($data = null) {
    // Generate 16 bytes (128 bits) of random data or use the data passed into the function.
    $data = $data ?? random_bytes(16);
    assert(strlen($data) == 16);

    // Set version to 0100
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
    // Set bits 6-7 to 10
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80);

    // Output the 36 character UUID.
    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}
//$myuuid = guidv4();
?>
